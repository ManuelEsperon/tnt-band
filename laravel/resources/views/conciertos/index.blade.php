@extends('templates.master')

@section('title')
Listado de conciertos
@stop

@section('central')
<h2>Próximos conciertos</h2>
<table class="table">
    <thead>
        <tr>
            <td scope="col">Conciertos</td>
            <td scope="col">
                @auth
                <a class="btn btn-warning"href="" >Añadir concierto</a>
                @endauth
            </td>
        </tr>
    </thead>
    <tbody>
        @foreach($conciertos as $concierto)
        <tr>
            <th scope="row">{{$concierto->address}}</th>
            <th scope="row ">
                <div class="d-flex justify-content-between">
                    <a class="btn btn-primary" href="http://maps.google.com/?ie=UTF8&hq=&ll={{ $concierto->latitud }},{{ $concierto->longitud }}&z=13" target="_blank">Google Maps</a>
                    <a class="btn btn-primary"href="http://maps.apple.com/?ll={{ $concierto->latitud }},{{ $concierto->longitud }}" target="_blank">Apple Maps</a>
                    @auth
                    <a class="btn btn-warning"href="" >Eliminar concierto</a>
                    @endauth
                    <a class="btn btn-primary" href="#">Más información</a>
                </div>
            </th>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $conciertos->links() !!}

@stop