<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConciertosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conciertos', function (Blueprint $table) {
            $table->id();
            $table->dateTime('dateTime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('address',150)->comment('Show address');
            $table->string('latitud', 20)->comment("Latitud")->nullable();
            $table->string('longitud', 20)->comment("Longitud")->nullable();
            $table->string('sign', 100)->comment("Event sign.")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conciertos');
    }
}
