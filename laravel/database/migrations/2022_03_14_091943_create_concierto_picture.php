<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConciertoPicture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concierto_picture', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('concierto_id');
            $table->unsignedBigInteger('picture_id');
            $table->timestamps();

            // Relaciones con otras tablas
            $table->foreign('concierto_id')->references('id')->on('conciertos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concierto_picture');
    }
}
