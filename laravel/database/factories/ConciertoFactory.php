<?php

namespace Database\Factories;

use App\Models\Concierto;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConciertoFactory extends Factory
{
    protected $model = Concierto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $dateTime = $this->faker->dateTimeBetween('-5 years', '+2 years');
        $address = $this->faker->address();
        $lat = $this->faker->latitude();
        $lon = $this->faker->longitude();
        // $sign = $this->faker->image('public/storage/images',640,480, null, false);
        $sign =  "cartel_por_defecto.jpg";        
        return [
            'dateTime' => $dateTime,
            'address' => $address,
            'latitud' => $lat,
            'longitud' => $lon,
            'sign' => $sign
        ];
    }
}
