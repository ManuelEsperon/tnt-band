<?php

namespace Database\Factories;

use App\Models\Picture;
use Illuminate\Database\Eloquent\Factories\Factory;

class PictureFactory extends Factory
{
    protected $model = Picture::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $description = $this->faker->paragraph(2, true);
        $date = $this->faker->dateTime();
        // $image = $this->faker->image('public/storage/images',640,480, null, false);
        $image =  "cartel_por_defecto.jpg";
        return [
            'description' => $description,
            'dateTime' => $date,
            'sign' => $image
        ];
    }
}
