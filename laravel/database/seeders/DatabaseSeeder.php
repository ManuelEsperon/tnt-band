<?php

namespace Database\Seeders;

use App\Models\Concierto;
use App\Models\Picture;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Concierto::factory(15)->create();
        User::factory(15)->create();
        Picture::factory(15)->create();
        
    }
}
