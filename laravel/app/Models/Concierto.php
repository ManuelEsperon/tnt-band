<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Concierto extends Model
{
    use HasFactory;
    // massive assignment attributes
    protected  $fillable = array('dateTime','address','lat','lon','sign');
}
